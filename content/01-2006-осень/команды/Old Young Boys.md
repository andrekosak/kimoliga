---
title: Old Young Boys
date: 2006-09-20
tags: ["состав", "Old Young Boys","2006-осень"]
---

{{< figure src="https://lh3.googleusercontent.com/2b750dlaw76qKcmhMenoqRxcccqj7PVK6Gkxr108zQGSgZIKQalM4w0E6-S1a3E6zbT4wwUyouWJwq-gi4Vmo4KvCoEkFeIBR1Ep0wxb_8i5uMbMF96_sYl-GFibpCd4cl-J3x1l7AO-qroRWBmivZMt6CSKBCREKIvaiQYshKcdpImvz1bZ5wAG5cAh_ro5YHOhIoJXbQMhGw0C7QxuVDS-UVtc7sJ2rv1rJNIluQn3hpE3RyjjFdhU6bCVX__usesMWceuMIy8nLsZ9JYU-36bcYJYXLtinCaj8PpWO2W-PU1fbpD4omfCN2dIWtcM8fIG-JPyPYJmfpYGz5lUFEq6PZW31eHYly_2s7Q-dUpZ5u-0tmDlafGT689rV00JLeXiWQQpLrRH-FDng4R6_Ncin9VSr6HCKB3PuzZNukHe5-IL3QbFMccwafz-nVRB9IvjqoZrbhhpzDHbOt9IQWRbMlZIxz3H3dW_8tUkGkOOWvNtcMHdYWrNhp4-bAhL5bkttsP4QQjwhZUkf2vB2A1tG8JaHuImWNqcBw1xvYZ_qyR8fJ7mXh0x0GqedaZrlpAkLBnhvTe2AuP2Xvu5HekWS6uKR1xFVxd4U74ilTYoTO_fJP3KIlXwif3LwjY5D-eQBtNtuFnGMXeM1vpIkd7hn8cFF1IVcwwpgzxy=w900-h598-no" >}}

---

{{<table>}}
| #  | Имя | Курс |
|----|-----|------|
| 1 | Кульбаба Тарас (к) | 4 МП  | 
| 2 | Зеленцов Леонид    | 5 МВ  | 
| 3 | Лясота Ярослав     | 5 МЕВ | 
| 4 | Харьковский Леонид | 6 МЕВ | 
| 5 | Абасов Алексей     | 2 МБ  | 
| 6 | Пищик Назар        | 2 МБ  | 
| 7 | Швец Сергей        | 5 МВ  | 
| 8 | Иващенко           | 4 МБ  | 
{{</table>}}