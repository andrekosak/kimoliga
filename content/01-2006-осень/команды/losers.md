---
title: Losers
date: 2006-09-20
tags: ["состав", "Losers","2006-осень"]
---

{{< figure src="https://lh3.googleusercontent.com/6tRb3aFcv77sRgLirhecJept1Nvwecu4bhY1dgyqoDS8n4gLl4CjDYO1fr9JyvjCkEb7YVMGgMlBa71rq93MFilpBkFdLDtJScrOJj-R0OPaCfqJbb59kBrHrB_TqfZXoF33ifTxWyujRpIe6c06Ep3RSK60Pa_yb9zGuFiwTcWWZXdtuKT3S0xx12DHFc_uRpFmPOFe2VaTmPUUDre3lXNEkZCJNGXRwH09gMigDvB9WE8__cpo430kW6aNQ9WN3gKm-BsWqnSnspnQ9hJmxSwHYa5syFCEyEV-rH_XWG8I3okKMqGba6PNK0DiRcdt80eDamo9kgoAJtY3eFezGjbFiK7FrKIJOh44qbass5CtKn__3fLzIJU5cljKFVtuQpLV-t6ZcyF1Cl25AHetozEOzTNjS_uMtrkGwLrvtIdpHbGpi6j5QtN5-VEB3VBE6cA8F42OW0m5T0cd3m68VNV3TLIzydRZHHfVDltRfQBMf1B8XDFDfHGIov3rEwRvuZyGRGAvjhkVucq64rkLlC1jJLFG4v_DWPni5yd9ucSlrgrVb6s5Kd-Q_u-sS-YtgAwBJSsjnbISMBYrCr3YEk8Jw72wPEPZpVsMQ608Ja9MvC5Gs-Je3lqa0Hf4rVNA94OXkkI-MPZ6oQEMDUGFbqV_3mqT3QAtrjn3mlvU=w900-h598-no" >}}

---

{{<table>}}
| #  | Имя | Курс |
|----|-----|------|
| 1 | Четвериков Антон (к) | 4 МЭО | 
| 2 | Бурданов Олег        | 3 МО  | 
| 3 | Вандер Михаил        | 4 МЭО | 
| 4 | Зыскинд Илья         | 4 МЭО | 
| 5 | Пилипенко Борис      | 4 МЭО | 
| 6 | Клыков Михаил        | 4 МЭО | 
| 7 | Гармаш Алексей       | 4 МЭО | 
| 8 | Павлюк Виталий       | 4 МЭО |
{{</table>}}