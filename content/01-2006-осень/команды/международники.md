---
title: Международники
date: 2006-09-20
tags: ["состав", "Международники","2006-осень"]
---

{{< figure src="https://lh3.googleusercontent.com/8DuYsZb-Sx5zk5wQrajoGWsj9Ji8S3kW7WA4-CVpPXQg6jUYMsJJ-r19-weSZs1KT7X4Qs40x6MA5eQNCHAAI54cII-M5Mni2PTLdXCExCeWKg1SI3DXd16jnVh_nmP_ZQjSRDnze0Az8-hjOisCdnuTQM3ueuS5h2h0frnWMWTCALo5tjk4WDuqRX5SE-sDYbEhpTMwPr5sRC4oJzncqsaXaPbM9oJxlN9ClG_l3aYhlGS6ws8IrWXEwlVapl0VYCU_FWy3swKNYqSSiRSj0AO-JBjLAyXbnYoKBjUGbMD6bhXJiYCwORUa3bqe0SLkYc8b2YhYyhRnlbxR1Q5zpwBGPv1iUYGBYijlgqib8E5vhwB83Rp-pfl7xKIGwpcyAKcE05WjExdBBv6KSuofpIfUOlLcgSjWR8zphFLH9FyssnemU-TQUU_l7s1TIWdmaXsznWQQvVbp18j96lycmSWLyOBcr3UV-F_KmOi0OLnqmGAUApqEeH3HicaooL-t5HBkwsOXfBCVVZYrmSFLF2llaCGxPMwgSRNbicpZfo-7kYCK1Pr8cIe64_U1xgsk3d4JkAIWu-W9vglTRT7ChU7RA0w98GgC8oR_4E1mrE9kCEI0j_7tMSVBNiG7Kcr3z5GJFJBI130IXmZqTjR2pWN5xcrcJ2VDXBE1fA6q=w900-h598-no" >}}

---

{{<table>}}
| #  | Имя | Курс |
|----|-----|------|
| 1  | Соронович Андрей (к) | 1 МП | 
| 2  | Лукинов Антон        | 1 МП | 
| 3  | Руднев Евгений       | 1 МП | 
| 4  | Власюк Виталий       | 1 МП | 
| 5  | Архипов Александр    | 1 МП | 
| 6  | Костов Игорь         | 1 МП | 
| 7  | Дордоль Адриан       | 1 МП | 
| 8  | Шутенко Ярослав      | 1 МП | 
| 9  | Черников Тарас       | 1 МП | 
| 10 | Попко Евгений        | 1 МП | 
| 11 | Гон Максим           | 1 МП | 
{{</table>}}