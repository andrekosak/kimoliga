---
title: FC DOTA Allstars
date: 2006-09-20
tags: ["состав", "FC DOTA Allstars","2006-осень"]
---

{{< figure src="https://lh3.googleusercontent.com/yNq_zT-3GGq5HkJSVdxuFQoZghU7yod2MRMkx0laJu8SvqmAIOpHbWvg0JEqUKh6QHBxx3VJA0RoTp7AOh07HPRAjkwW71862XuTqrXRgKmkmKsuCyDjE3dd1azAwSCncuBtqoaqvwEuqVRWQBpdBSRS3tbC5fb7a1M7Lqztp0WP3z-U6kniZYBhXd1lPh_eYf6DFZC4s-vyqbL_kl0PpG56C2qCvhWCfzD7mOjLEhhEnVaCAmtKzsbLD14qeGVgmUsqMjiJrzOAYnCwK186tvIKgX4jASNbIClNgSL17Q1pjcq72geJZO_Ce4bM87VxB_givi2wVUfFP17lohRsP8NKm_ACivLHxFGRPrqLHTlAqGZlZ946OOSIFRl3o9_jVu5_abmatgUU3NBopTxlllnqQQ37IfjmgdD8RUxUOCS73r9LG0G2LoPT07goTM1BjZbOMLU1YoDE6TxyxYdBCEZwUr4gPMNrO-amkoa1fgcpaHdv1Ula2PLNzOyfaodaLogmfqZlwq3CSYHOMqMdNsAeQXld6XNGC6iOfMjQRJuPdCKswAm4xV0V1rfRlvtwHwOVlJw6Upz24iO1oYCEceL0ht_Qq_nQsB5J5WCz0qNWXQW6sJ-zcbRUVcUtP1FaHY4ejNeeCfwAq4ixlr0IWz4tjM6K-2hF84fb0Dv_=w900-h598-no" >}}

---

{{<table>}}
| #  | Имя | Курс |
|----|-----|------|
| 1 | Проневич Владимир (к) | 3 МО | 
| 2 | Бабич Алексей         | 3 МО | 
| 3 | Карпышын Роман        | 3 МИ | 
| 4 | Пожидаев Олег         | 3 МО | 
| 5 | Такса Виктор          | 2 МО | 
| 6 | Тименко Анатолий      | 3 МИ | 
| 7 | Курбанов Асан         | 3 МО | 
| 8 | Волынец Александр     | 3 МО | 
| 9 | Бондарь Алексей       | 3 МО | 
{{</table>}}