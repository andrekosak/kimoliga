---
title: Любi Друзi
date: 2008-03-28
tags: ["состав", "Любi Друзi","2008-весна"]
---

{{< figure src="/teams/logo_teams_ld.jpg" >}}

{{<table>}}
|   |                     |                     |
|----|----------------------|----------------------|
|1. |Мясоедов Антон       |4 курс МБ (капитан)  |
|2. |Косяк Андрей         |4 курс МБ            |
|3. |Шапран Алексей       |3 курс МЭО (вратарь) |
|4. |Костанецкий Владимир |4 курс МБ            |
|5. |Кавицкий Алексей     |4 курс МБ            |
|6. |Горбенко Игорь       |4 курс МБ            |
{{</table>}}
