---
title: MYMY
date: 2008-03-28
tags: ["состав", "MYMY","2008-весна"]
---

{{<table>}}
|    |                    |                     |
|-----|---------------------|----------------------|
|1.  |Баранов Вадим       |4 курс МП (вратарь)  |
|2.  |Билянский Александр |4 курс МЭО           |
|3.  |Кобец Глеб          |4 курс МЭО           |
|4.  |Колесник Ярослав    |4 курс МЭО           |
|5.  |Колюка Юрий         |4 курс МЭО           |
|6.  |Нетребенко Валерий  |4 курс МП            |
|7.  |Нефед Валерий       |4 курс МЭО           |
|8.  |Питцик Игорь        |4 курс МЭО           |
|9.  |Романенко Александр |4 курс МЭО           |
|10. |Терещенко Юрий      |4 курс МЭО (капитан) |
|11. |Пастовенский Антон  |4 курс МЭО           |
|12. |Ткаченко Александр  |4 курс МО            |
{{</table>}}