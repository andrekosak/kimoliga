---
title: ВЛК Весна 2007
subtitle: 31 марта - 22 апреля 2007г.
---

#### 31 марта - 22 апреля 2007г.

**ЭДЕЛЬВЕЙС - ЧЕМПИОН**

**Второе место - Орион**

**Третье место - Дружба народов**

Дополнительные ссылки:

* [Фото: Финал и награждение](https://goo.gl/photos/qdNbWL1JEjdE2Dhz7)
* [Фото: команды](https://goo.gl/photos/cJSMLJ5sRHErTBTFA)
* [Фото с игр](https://goo.gl/photos/KjkSGYr63JsmQ77r7)
* [Афиша](https://goo.gl/photos/R6vdoeoGUHqe1Pa9A)
