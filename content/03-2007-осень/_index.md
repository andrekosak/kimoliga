---
title: ВЛК Осень 2007
subtitle: 29 сентября - 21 октября 2007г.
---

#### 29 сентября - 21 октября 2007г.

**ONORTA - ЧЕМПИОН**

**Второе место - DreamTeam**

**Третье место - Эдельвейс**

Дополнительные ссылки:

* [Фото сезон 03: Финал и награждение](https://goo.gl/photos/opFZwVKknH2kaaNJ6)
* [Фото сезон 03: команды](https://goo.gl/photos/VvaAQkjZYgyvNyZH6)
* [Фото сезон 03: игры](https://goo.gl/photos/9yB9LVW5Z2J4DR2V8)
* [Фото сезон 03: Горан Говранчич](https://goo.gl/photos/K9fzZeWCqvTKMeZC7)
* [Фото сезон 03: by Natali Budzei](https://goo.gl/photos/b93zGXs3sHCMZHS89)
* [Видео сезон 03](https://goo.gl/photos/9PwhkdwMnyfRRtQR6)
* [Афиша](https://goo.gl/photos/JPTw3juTJtVMUWfS6)

